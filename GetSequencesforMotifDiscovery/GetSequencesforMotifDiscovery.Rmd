---
title: "GetSequencesforMotifDiscovery"
author: "Beth Krizek"
date: "8/15/2019"
output: html_document
---

## Introduction
ChIP-Seq peaks for genome-wide ANT binding sites in stage 6/7 flowers were identified using Visual Analytics in IGB. Here we obtain the sequences of these peaks in FASTA format for Motif Discovery using MEME-ChIP from The MEME Suite. 

## Methods
* Download Arabidopsis genomic sequences and unzip
1. https://plants.ensembl.org/Arabidopsis_thaliana/Info/Index
2. ftp://ftp.ensemblgenomes.org/pub/plants/release-44/fasta/arabidopsis_thaliana/dna/
* Arabidopsis_thaliana.TAIR10.dna.chromosome1.fa.gz
* Arabidopsis_thaliana.TAIR10.dna.chromosome2.fa.gz
* Arabidopsis_thaliana.TAIR10.dna.chromosome3.fa.gz
* Arabidopsis_thaliana.TAIR10.dna.chromosome4.fa.gz
* Arabidopsis_thaliana.TAIR10.dna.chromosome5.fa.gz

* Edit files of ANT ChIP-Seq peaks so that "1,2,3,4,or 5" present in first column"
```{bash}
sed 's/Chr1/1/' '../FindClosestGene/data/chr1.1000.bed' > 'results/rchr1.1000.bed'
sed 's/Chr2/2/' '../FindClosestGene/data/chr2.1000.bed' > 'results/rchr2.1000.bed'
sed 's/Chr3/3/' '../FindClosestGene/data/chr3.1000.bed' > 'results/rchr3.1000.bed'
sed 's/Chr4/4/' '../FindClosestGene/data/chr4.1000.bed' > 'results/rchr4.1000.bed'
sed 's/Chr5/5/' '../FindClosestGene/data/chr5.1000.bed' > 'results/rchr5.1000.bed'
```

* Get FASTA sequences for peaks
```{bash}
fastaFromBed -fi 'data/Arabidopsis_thaliana.TAIR10.dna.chromosome.1.fa' -bed 'results/rchr1.1000.bed' -fo './results/chr1peaksseq.fasta'
fastaFromBed -fi 'data/Arabidopsis_thaliana.TAIR10.dna.chromosome.2.fa' -bed 'results/rchr2.1000.bed' -fo './results/chr2peaksseq.fasta'
fastaFromBed -fi 'data/Arabidopsis_thaliana.TAIR10.dna.chromosome.3.fa' -bed 'results/rchr3.1000.bed' -fo './results/chr3peaksseq.fasta'
fastaFromBed -fi 'data/Arabidopsis_thaliana.TAIR10.dna.chromosome.4.fa' -bed 'results/rchr4.1000.bed' -fo './results/chr4peaksseq.fasta'
fastaFromBed -fi 'data/Arabidopsis_thaliana.TAIR10.dna.chromosome.5.fa' -bed 'results/rchr5.1000.bed' -fo './results/chr5peaksseq.fasta'
```

* Make a single file of FASTA peak sequences 
```{bash}
cat './results/chr1peaksseq.fasta' './results/chr2peaksseq.fasta' './results/chr3peaksseq.fasta' './results/chr4peaksseq.fasta' './results/chr5peaksseq.fasta' > './results/allchrpeaksseq.fasta'
```

### Session info
```{r}
sessionInfo()
```

## Results
The output file "allchrpeaksseq.fasta" contains sequences in FASTA format for each ANT ChIP-Seq peak. This file will be used as input for MEME-ChIP.