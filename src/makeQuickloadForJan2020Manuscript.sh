#!/bin/bash

# create a quickload site in a new directory named
# krizeklab in the current working directory
# contents:
#
# krizeklab/
# ├── A_thaliana_Jun_2009
# │   ├── annots.xml
# │   └── genome.txt
# ├── contents.txt
# └── favicon.ico

D="krizeklab"
if [ ! -d $D ];
then
  mkdir $D
fi;
A="A_thaliana_Jun_2009"
if [ ! -d $D/$A ];
then
  mkdir $D/$A
fi;
chmod a+x $D
chmod a+x $D/$A
cp favicon.ico $D/.
curl http://igbquickload.org/quickload/contents.txt 2>/dev/null | grep A_thaliana_Jun_2009 > $D/contents.txt
curl http://igbquickload.org/quickload/A_thaliana_Jun_2009/genome.txt 2>/dev/null > $D/$A/genome.txt
makeAnnotsXml.py --manuscriptJan2020 > $D/$A/annots.xml
chmod -R a+r $D
