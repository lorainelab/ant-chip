#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=24:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load samtools

samtools merge s6s7-rna-ANT_VENUS.bam B2.bam B4.bam B5.bam

samtools merge s3-rna-ANT_VENUS.bam B6.bam B7.bam B8.bam

samtools merge s3-rna-AIL6_VENUS.bam B9.bam B10.bam B11.bam

for f in s*; do
    samtools index $f
done

